import uuid
from datetime import date

import aioredis
import httpx
import sanic.request
import ujson
import xmltodict
from sanic import Sanic, response
from sanic.exceptions import SanicException

from . import settings, exceptions, client

app = Sanic('service-app')


@app.post('/offers/search', strict_slashes=True)
async def search_offers(request):
    return response.json({
        'id': str(uuid.uuid4()),
        'status': 'pending',
        'items': []
    })


@app.get('/rates', strict_slashes=True)
async def get_rates(request: sanic.request.Request):
    if date_str := request.args.get('date'):
        try:
            dt = date.fromisoformat(date_str)
        except ValueError:
            raise exceptions.ValidationError('Wrong date format')

        if dt > date.today():
            raise exceptions.ValidationError('Date should be less or equal today date')
    else:
        dt = date.today()

    redis = request.app.ctx.redis
    if rates := await redis.get(f'currency_rates:{dt.isoformat()}'):
        return response.json({
            'date': dt.isoformat(),
            'rates': ujson.loads(rates)
        })

    async with client.HTTPClient() as cl:
        url = settings.NATIONAL_BANK_URL.format(dt.strftime('%d.%m.%Y'))
        r: httpx.Response = await cl.get(url)

        if r.status_code != 200:
            raise SanicException('Cannot fetch rates from provider')

        parsed = xmltodict.parse(r.text)
        rates = {rate['title']: rate['description'] for rate in parsed['rates']['item']}

    await redis.set(f'currency_rates:{dt.isoformat()}', ujson.dumps(rates), ex=20 * 60)

    return response.json({
        'date': dt.isoformat(),
        'rates': rates
    })


@app.listener('before_server_start')
async def init_before(app, loop):
    app.ctx.redis = aioredis.from_url(settings.REDIS_URL, decode_responses=True)


@app.listener('after_server_stop')
async def cleanup(app, loop):
    await app.ctx.redis.close()


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8000, auto_reload=True)
