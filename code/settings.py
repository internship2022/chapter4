import os

REDIS_URL = os.environ.get('REDIS_URL')
NATIONAL_BANK_URL = 'https://www.nationalbank.kz/rss/get_rates.cfm?fdate={}'
