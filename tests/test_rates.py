from code import client
from unittest.mock import AsyncMock, MagicMock

import pytest
import ujson
from aioredis import Redis

from utils import load_file


@pytest.mark.parametrize('args, expected_status', [
    ('', 200),  # default date
    ('date=2022-02-20', 200),  # clear date
    ('date=202102-20', 422),  # wrong date format
    ('date=2032-02-20', 422),  # date greater then today
])
async def test_rates_validation(mocker, app, args, expected_status):
    async_mock = AsyncMock(return_value='{}')
    mocker.patch.object(Redis, 'get', side_effect=async_mock)

    request, response = await app.asgi_client.get(f'/rates?{args}')

    assert request.method == 'GET'
    assert response.status == expected_status


async def test_rates_failed(mocker, app):
    resp = MagicMock(status_code=500)
    mocker.patch.object(client.HTTPClient, 'get', return_value=resp)
    mocker.patch.object(Redis, 'get', side_effect=AsyncMock(return_value=None))
    mocker.patch.object(Redis, 'set', side_effect=AsyncMock())

    request, response = await app.asgi_client.get('/rates')

    assert request.method == 'GET'
    assert response.status == 500


async def test_rates_ok(mocker, app):
    expected_response = ujson.loads(load_file('tests/data/rates_expected.json'))
    provider_response = load_file('tests/data/rates_provider_response.xml')

    resp = MagicMock(status_code=200, text=provider_response)

    mocker.patch.object(client.HTTPClient, 'get', return_value=resp)
    mocker.patch.object(Redis, 'get', side_effect=AsyncMock(return_value=None))
    mocker.patch.object(Redis, 'set', side_effect=AsyncMock())

    request, response = await app.asgi_client.get('/rates?date=2022-02-20')

    assert request.method == 'GET'
    assert response.status == 200
    assert response.json == expected_response
